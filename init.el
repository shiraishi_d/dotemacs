(setq load-path (cons (expand-file-name "~/.emacs.d/lisp") load-path))
(setq ring-bell-function (lambda ()));ベル無音、フラッシュ解除
(setq delete-auto-save-files t);終了時にオートセーブファイルを消す
(setq delete-auto-save-save nil);終了時にオートセーブファイルを消す
(setq backup-inhibited t);バックアップを作らない
(setq column-number-mode t);カーソルの位置が何文字目か表示
(setq kill-whole-line t);C-kで行全体を削除
(setq require-final-newline t);最終行に必ず一行挿入
(recentf-mode);最近使ったファイルを保存M-x recntf-open-filesでひらける
(transient-mark-mode t);リージョンに色つけ
(show-paren-mode t);対応する括弧を表示
(setq default-tab-width 2 );tab width
(global-font-lock-mode t);色つけ
;;(global-hl-line-mode 1);現在行に色 
(setq inhibit-startup-message t);初期表示画面を表示させない
(global-set-key "\M-g" 'goto-line); M-g で指定行へ移動
(define-key global-map "\C-h" 'delete-backward-char); C-h でカーソルの左にある文字を消す
(define-key global-map "\C-o" 'dabbrev-expand); C-o に動的略語展開機能を割り当てる
(setq dabbrev-case-fold-search nil) ; 大文字小文字を区別
;;自動chmod
(defun make-file-executable ()
  "Make the file of this buffer executable, when it is a script source."
  (save-restriction
    (widen)
    (if (string= "#!" (buffer-substring-no-properties 1 (min 3 (point-max))))
        (let ((name (buffer-file-name)))
          (or (equal ?. (string-to-char (file-name-nondirectory name)))
              (let ((mode (file-modes name)))
                (set-file-modes name (logior mode (logand (/ mode 4) 73)))
                (message (concat "Wrote " name " (+x)"))))))))
(add-hook 'after-save-hook 'make-file-executable)

;;日本語入力
;;(load-library "anthy")
;;(load-file "$HOME/.emacs.d/leim-list.el")
;;(setq default-input-method 'japanese-anthy)
(set-language-environment "Japanese")
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-buffer-file-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(setq coding-system-for-read 'utf-8)
(setq coding-system-for-write 'utf-8)


;;略称展開
(quietly-read-abbrev-file)
(setq save-abbrevs t)
(setq abbrev-file-name "~/.emacs.d/abbrev_defs")
(define-key esc-map  " " 'expand-abbrev) ;; M-SPC
;;c言語
(add-hook 'c-mode-common-hook
          '(lambda ()
             ;; センテンスの終了である ';' を入力したら、自動改行+インデント
             (c-toggle-auto-hungry-state 1)
             ;; RET キーで自動改行+インデント
             (define-key c-mode-base-map "\C-m" 'newline-and-indent)
))
;;ruby-mode
(autoload 'ruby-mode "ruby-mode" "Mode for editing ruby source files")
(setq auto-mode-alist
(append '(("\\.rb$" . ruby-mode)) auto-mode-alist))
(setq interpreter-mode-alist
  (append '(("^#!.*ruby" . ruby-mode)) interpreter-mode-alist))

;行末の空白やタブに色をつける
(defface my-face-b-1 '((t (:background "gray"))) nil)
(defface my-face-b-2 '((t (:background "gray26"))) nil)
(defface my-face-u-1 '((t (:foreground "SteelBlue" :underline t))) nil)
(defvar my-face-b-1 'my-face-b-1)
(defvar my-face-b-2 'my-face-b-2)
(defvar my-face-u-1 'my-face-u-1)

(defadvice font-lock-mode (before my-font-lock-mode ())
(font-lock-add-keywords
major-mode
'(("\t" 0 my-face-b-2 append)
("　" 0 my-face-b-1 append)
("[ \t]+$" 0 my-face-u-1 append)
)))
(ad-enable-advice 'font-lock-mode 'before 'my-font-lock-mode)
(ad-activate 'font-lock-mode)

;;色設定
(load-theme 'misterioso t)

;ディレクトリ移動でバッファを増やさない
(defun dired-my-advertised-find-file ()
  (interactive)
  (let ((kill-target (current-buffer))
	(check-file (dired-get-filename)))
    (funcall 'dired-advertised-find-file)
    (if (file-directory-p check-file)
	(kill-buffer kill-target))))
(defun dired-my-up-directory (&optional other-window)
  "Run dired on parent directory of current directory.
Find the parent directory either in this buffer or another buffer.
Creates a buffer if necessary."
   (interactive "P")
   (let* ((dir (dired-current-directory))
	  (up (file-name-directory (directory-file-name dir))))
     (or (dired-goto-file (directory-file-name dir))
	 ;; Only try dired-goto-subdir if buffer has more than one dir.
	 (and (cdr dired-subdir-alist)
	      (dired-goto-subdir up))
	 (progn
	   (if other-window
	       (dired-other-window up)
	     (progn
	       (kill-buffer (current-buffer))
	       (dired up))
	     (dired-goto-file dir))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; [PKG管理] パッケージ取得先追加
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") t)
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(package-initialize)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; [PKG導入]
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; インストールするパッケージ
(defvar my/favorite-packages
  '(
		  anything
		  recentf-ext
		  magit git-gutter
			slim-mode
			scss-mode
			coffee-mode
		  ))
;; my/favorite-packagesからインストールしていないパッケージをインストール
(dolist (package my/favorite-packages)
  (unless (package-installed-p package)
        (package-install package)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; [PKG機能利用] anything, recentf-ext, kill-ring
;;
;; M-x package-list-install
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; C-q で anything-for-files 起動
(global-set-key (kbd "C-h") 'anything-for-files)
;; recentf による保存数を 10000 に
(setq recentf-max-saved-items 10000)
;; 「バッファを替えた時ファイルを開いたとみなす」「Directoryも履歴に保存」する拡張
(require 'recentf-ext)
;; 遅延を短く
(setq anything-idle-delay 0.1)
(setq anything-input-idle-delay 0.1)
;; kill-ring の最大値。デフォルトは 30
(setq kill-ring-max 100)
;; M-y で kill-ring を表示
(global-set-key "\M-y" 'anything-show-kill-ring)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;slim
(require 'slim-mode)

;;scss
(autoload 'scss-mode "scss-mode")
(setq scss-compile-at-save nil) ;; 自動コンパイルをオフにする
(setq css-indent-offset 2)
(add-to-list 'auto-mode-alist '("\\.scss\\'" . scss-mode))

;;coffee
(require 'coffee-mode)
